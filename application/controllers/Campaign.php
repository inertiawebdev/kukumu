<?php 

	class Campaign extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}


		private $tbl = "campaign";

		function index(){

			$data = [
				'pic1' => $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 1 ])->row(),
				'pic2' => $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 2 ])->row(),
				'pic3' => $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 3 ])->row(),
				'pic4' => $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 4 ])->row(),
				'pic5' => $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 5 ])->row(),
				'pic6' => $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 6 ])->row(),
				'pic7' => $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 7 ])->row(),
				'pic8' => $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 8 ])->row(),
				'pic9' => $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 9 ])->row(),
				'vid10'=> $this->my_query->get_data('*'  , $this->tbl ,  ['id' => 10 ])->row(),
			];

			$isi = [
				'content' => $this->load->view('backend/campaign/campaign_index' , $data , true)
			];

			$this->load->view('backend/layout_all' , $isi );
		}

		function update($id){

			$current_img = $this->my_query->get_data('*' , $this->tbl , ['id' => $id]);
			$old_path = 'assets/img/'.$current_img->row()->file;

			if ($_FILES['file'.$id]['tmp_name'] != "") {
				
				$config['upload_path']          = './assets/img/';
			    $config['allowed_types']        = 'jpg|png|jpeg';

			    $this->load->library('upload', $config);

			    if ( ! $this->upload->do_upload('file'.$id) ) {
	                $error = array('error' => $this->upload->display_errors() );

	                $this->session->set_flashdata('result' , 'error');
	  				$this->session->set_flashdata('result_message' , $error['error'] );
					
	            } else {

					if ( file_exists( $old_path) ) {
						unlink($old_path);
					}

		            $data = array('upload_data' => $this->upload->data());
		            $datainsert =  [
		            	'file' => $data['upload_data']['file_name']
		            ];
		            
		            $this->my_query->insert_for_id($this->tbl , ['id' => $id] , $datainsert);
		            
		            $this->session->set_flashdata('result', 'success');
  					$this->session->set_flashdata('result_message', 'Berhasil di upload ');
		    	}

			}

	    	redirect( base_url('Campaign') );

		}


		function upload_video(){

			$config['upload_path']          = './assets/video/';
		    $config['allowed_types']        = 'mp4';
		    $config['overwrite']			= true;
		    // $config['max_size']             = ;
		    $this->load->library('upload', $config);

		    if ( ! $this->upload->do_upload('file10')) {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('result' , 'error');
  				$this->session->set_flashdata('result_message' , $error['error'] );
            } else {
	            $data = array('upload_data' => $this->upload->data());

	            $this->my_query->insert_for_id($this->tbl , ['id' => 10] , ['file'=> $data['upload_data']['file_name'] ]);


	            $this->session->set_flashdata('result' , 'info');
  				$this->session->set_flashdata('result_message' , 'Video berhasil di upload.');
	    	}

	    	redirect( base_url('Campaign') );

		}

	}

 ?>