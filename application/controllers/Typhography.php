<?php 

	class Typhography extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "stories_typography";

		function index($edit = ""){

			$data = [
				'typo' => $this->my_query->get_data('*' , $this->tbl , ['id' => 1])->row()
			];

			if ($edit != "") {
				$isi = [
					'content' => $this->load->view('backend/stories/stories_typography_edit' , $data , true)
				];
			} else {
				$isi = [
					'content' => $this->load->view('backend/stories/stories_typography' , $data , true)
				];
			}

			$this->load->view('backend/layout_all' , $isi );

		}

		function update(){

			$data = [
				'stories_desc' =>$_POST['stories_desc']
			];

			$this->my_query->insert_for_id($this->tbl , ['id' => 1] , $data);

			$this->session->set_flashdata('result' , 'info');
	  		$this->session->set_flashdata('result_message' , 'Data Berhasil di Rubah');

	  		redirect( base_url('Typhography') );

		}

	
	}

 ?>