<?php 

	class Intro extends CI_Controller
	{

		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "keperluanku_intro";

		function index($edit = ""){

			$data = [
				'intro' => $this->my_query->get_data('*' , $this->tbl , ['keperluanku_intro_id' => 1])->row()
			];

			if ($edit != "") {

				$isi = [
					'content' => $this->load->view('backend/keperluanku/intro_keperluan_edit' , $data , true),
				];

			} else {

				$isi = [
					'content' => $this->load->view('backend/keperluanku/intro_keperluan' , $data , true),
				];

			}

			$this->load->view('backend/layout_all' , $isi );

		}

		function update_process(){

			$data=  [
				'desc1' => $_POST['desc1'],
				'desc2' => $_POST['desc2']
			];

			$this->my_query->insert_for_id($this->tbl ,  ['keperluanku_intro_id' => 1] , $data);

			
			$this->session->set_flashdata('result' , 'info');
  			$this->session->set_flashdata('result_message' , 'Data Home berhasil di rubah!' );
			redirect( base_url('Intro/index/') );




		}
	}
?>