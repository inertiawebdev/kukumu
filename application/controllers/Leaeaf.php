<?php 

	class Leaeaf extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "leaeaf";

		function index($edit = ""){

			$data = [
				'leaeaf' => $this->my_query->get_data('*' , $this->tbl , ['id' => 1])->row()
			];

			if ($edit != "") {
				$isi = [
					'content' => $this->load->view('backend/keperluanku/leaeaf_edit' , $data , true)
				];
			} else {
				$isi = [
					'content' => $this->load->view('backend/keperluanku/leaeaf' , $data , true)
				];
			}

			$this->load->view('backend/layout_all' , $isi );

		}

		function update(){

			$data = [
				'description' => $_POST['desc'],
				'desc_1' => $_POST['desc_1'],
				'desc_2' => $_POST['desc_2'],
				'desc_3' => $_POST['desc_3'],	
				'desc_4' => $_POST['desc_4'],
			];

			$this->my_query->insert_for_id($this->tbl , ['id' => 1] , $data);

			$this->session->set_flashdata('result' , 'info');
	  		$this->session->set_flashdata('result_message' , 'Data Berhasil di Rubah');

	  		redirect( base_url('Leaeaf') );

		}

	
	}

 ?>