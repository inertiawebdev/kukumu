<?php 

	class Vision extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "stories_vision";

		function index(){

			$data = [
				'vis' => $this->my_query->get_data('*' , $this->tbl , ['id' => 1])->row()
			];

			$isi = [
				'content' => $this->load->view('backend/stories/stories_vision' , $data , true)
			];

			$this->load->view('backend/layout_all' , $isi );

		}

		function update( $type ){

			if ($type=="vision") {
				
				$data = [
					'vision_paragraph_1' => $_POST['vision_paragraph_1'],
					'vision_paragraph_2' => $_POST['vision_paragraph_2'],
					'vision_paragraph_3' => $_POST['vision_paragraph_3'],
				];

				$this->my_query->insert_for_id($this->tbl , ['id' => 1] , $data);

				$this->session->set_flashdata('result' , 'info');
	  			$this->session->set_flashdata('result_message' , 'Visi sudah diperbarui!');
	  			redirect( base_url('Vision') );

			} else if($type == "mission"){

				$data = [
					'mission_paragraph_1' => $_POST['mission_paragraph_1'],
					'mission_paragraph_2' => $_POST['mission_paragraph_2'],
					'mission_paragraph_3' => $_POST['mission_paragraph_3'],
				];

				$this->my_query->insert_for_id($this->tbl , ['id' => 1] , $data);

				$this->session->set_flashdata('result' , 'info');
	  			$this->session->set_flashdata('result_message' , 'Mission sudah diperbarui!');
	  			redirect( base_url('Vision') );


			} else {
				$this->session->set_flashdata('result' , 'error');
	  			$this->session->set_flashdata('result_message' , 'Tidak ditemukan');
	  			redirect( base_url('Vision') );
			}


		}
	}

 ?>