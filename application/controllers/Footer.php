<?php 

	class Footer extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "footer";

		function index(){

			$data = [
				'foot' 		=>  $this->my_query->get_data('*' , $this->tbl , ['id' => 1])->row(),
			];

			$isi = [
				'content' 	=> 	$this->load->view('backend/footer/footer_index' , $data , true),
			];
			
			$this->load->view('backend/layout_all' , $isi );

		}

		function update(){
			$data=  [
				'tokped_link' => $_POST['tokped_link'],
				'ig_link'     => $_POST['instagram_link'],
				'website'     => $_POST['web_link'],
				'mail'        => $_POST['mail_link']
			];

			$this->my_query->insert_for_id($this->tbl ,  ['id' => 1] , $data);

			
			$this->session->set_flashdata('result' , 'info');
  			$this->session->set_flashdata('result_message' , 'Data Footer berhasil di rubah!' );
			redirect( base_url('Footer') );
		}
	}