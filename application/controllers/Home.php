<?php 

	class Home extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "home";

		function index($edit = ""){

			$data = [
				'home' => $this->my_query->get_data('*' , $this->tbl , ['home_id' => 1])->row()
			];

			if ($edit != "") {
				$isi = [
					'content' => $this->load->view('backend/homepage/home_index_edit' , $data , true)
				];
			} else {
				$isi = [
					'content' => $this->load->view('backend/homepage/home_index' , $data , true)
				];
			}

			$this->load->view('backend/layout_all' , $isi );

		}

		function update_data(){

			$data = [
				'headline'    => $_POST['headline'],
				'description' => $_POST['desc']
			];

			$this->my_query->insert_for_id($this->tbl , ['home_id' => 1] , $data);


			$this->session->set_flashdata('result' , 'info');
  			$this->session->set_flashdata('result_message' , 'Data Home berhasil di rubah!' );
	    	redirect( base_url('Home') );

		}
	}

 ?>