<?php 

	class Dashboard extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
        	$this->load->helper('indodate');
		}

		function index(){

			$isi =[
				
				'visitors'		  => $this->my_query->query("SELECT count(visitor_user_agent) as jumlah_visitor , date as tgl from visitors group by date")->result(),
			];


			$isi = [
				'content' => $this->load->view('backend/dashboard/dashboard' , $isi , true)
			];

			$this->load->view('backend/layout_all' , $isi );


		}
	}

 ?>