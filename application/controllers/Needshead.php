	<?php 

	class Needshead extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "keperluanku_header";

		function index($edit = ""){

			$data = [
				'keperluanku' => $this->my_query->get_data('*' , $this->tbl , ['id' => 1])->row()
			];

			if ($edit == "") {
				$isi = [
					'content' => $this->load->view('backend/keperluanku/keperluanku_header' , $data , true)
				];
			} else {
				$isi = [
					'content' => $this->load->view('backend/keperluanku/keperluanku_header_edit' , $data , true)
				];
			}
			
		
			$this->load->view('backend/layout_all' , $isi );

		}

		function update(){

			$datainsert = [
				'description'                  => $_POST['desc'],
			];

			if ($_FILES['name']['tmp_name'] != "") {
				
				$config['upload_path']          = './assets/img/';
			    $config['allowed_types']        = 'jpg|png|jpeg';
			    $this->load->library('upload', $config);

			    if ( ! $this->upload->do_upload('name')) {
	                $error = array('error' => $this->upload->display_errors());

	                $this->session->set_flashdata('result' , 'error');
	  				$this->session->set_flashdata('result_message' , $error['error'] );

	  				redirect( base_url('Needshead/index/edit') );
	            } else {
		            $data = array('upload_data' => $this->upload->data());

		            $datainsert['picture'] = $data['upload_data']['file_name'];

		            $this->session->set_flashdata('result' , 'info');
	  				$this->session->set_flashdata('result_message' , 'Data Berhasil di simpan');
		    	}


			}

		    $this->my_query->insert_for_id($this->tbl , ['id' => 1] , $datainsert );
	    	redirect( base_url('Needshead') );

	    	

		}
	}

 ?>