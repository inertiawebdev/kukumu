<?php 

	class Cdn extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "keperluanku_cdn";

		function index($edit = ""){

			$data = [
				'cdn' => $this->my_query->get_data('*' , $this->tbl , ['id' => 1])->row()
			];

			
			$isi = [
				'content' => $this->load->view('backend/keperluanku/cdn_index' , $data , true)
			];
			

			$this->load->view('backend/layout_all' , $isi );

		}

		function update(){

			$data = [
				'text1'    => $_POST['text1'],
				'text2' => $_POST['text2']
			];

			$this->my_query->insert_for_id($this->tbl , ['id' => 1] , $data);


			$this->session->set_flashdata('result' , 'info');
  			$this->session->set_flashdata('result_message' , 'Data Cdn berhasil di rubah!' );
	    	redirect( base_url('Cdn') );

		}
	}

 ?>