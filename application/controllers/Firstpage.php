<?php 

	class Firstpage extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "playlist";

		function index(){

			$data=  [
				'video' => $this->my_query->get_data('*', $this->tbl , ['id' =>1] )->row()	
			];

			$isi = [
				'content' => $this->load->view('backend/firstpage/firstpage' , $data , true)
			];

			$this->load->view('backend/layout_all' , $isi );

		}

		function upload_video(){

			$config['upload_path']          = './assets/video/';
		    $config['allowed_types']        = 'mp4';
		    $config['overwrite']			= true;
		    // $config['max_size']             = ;
		    $this->load->library('upload', $config);

		    if ( ! $this->upload->do_upload('video')) {
                $error = array('error' => $this->upload->display_errors());

                $this->session->set_flashdata('result' , 'error');
  				$this->session->set_flashdata('result_message' , $error['error'] );
            } else {
	            $data = array('upload_data' => $this->upload->data());

	            $this->my_query->insert_for_id($this->tbl , ['id' => 1] , ['video_directory'=> $data['upload_data']['file_name'] ]);


	            $this->session->set_flashdata('result' , 'info');
  				$this->session->set_flashdata('result_message' , 'Video berhasil di upload.');
	    	}

	    	redirect( base_url('Firstpage') );

		}
	}

 ?>