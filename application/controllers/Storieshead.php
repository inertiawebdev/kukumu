<?php 

	class Storieshead extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "stories_header";

		function index($edit = ""){

			$data = [
				'story' => $this->my_query->get_data('*' , $this->tbl , ['id' => 1])->row()
			];

			if ($edit != "") {
				$isi = [
					'content' => $this->load->view('backend/stories/stories_head_edit' , $data , true)
				];
			} else {
				$isi = [
					'content' => $this->load->view('backend/stories/stories_head' , $data , true)
				];
			}

			$this->load->view('backend/layout_all' , $isi );

		}

		function update(){

			$datainsert = [
				'story_desc'                  => $_POST['desc'],
				'story_desc_first_paragraph'  => $_POST['desc1'],
				'story_desc_second_paragraph' => $_POST['desc2']
			];

			if ($_FILES['descpicture']['tmp_name'] != "") {
				
				$config['upload_path']          = './assets/img/';
			    $config['allowed_types']        = 'jpg|png|jpeg';
			    // $config['max_size']             = ;
			    $this->load->library('upload', $config);

			    if ( ! $this->upload->do_upload('descpicture')) {
	                $error = array('error' => $this->upload->display_errors());

	                $this->session->set_flashdata('result' , 'error');
	  				$this->session->set_flashdata('result_message' , $error['error'] );

	  				redirect( base_url('Storieshead/index/edit') );
	            } else {
		            $data = array('upload_data' => $this->upload->data());

		            $datainsert['story_picture'] = $data['upload_data']['file_name'];

		            $this->session->set_flashdata('result' , 'info');
	  				$this->session->set_flashdata('result_message' , 'Data Berhasil di simpan');
		    	}


			}

		    $this->my_query->insert_for_id($this->tbl , ['id' => 1] , $datainsert );
	    	redirect( base_url('Storieshead') );

		}
	}

 ?>