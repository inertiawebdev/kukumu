<?php 

	class Studio extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
        	$this->load->model('my_query');
		}

		private $tbl = "studio";

		function index(){

			$data = [
				'studio' => $this->my_query->get_data('*' , $this->tbl , ['id' => 1])->row()
			];

			$isi = [
				'content' => $this->load->view('backend/studio/studio_index' , $data , true)
			];

			$this->load->view('backend/layout_all' , $isi );

		}

		function update(){

			$data = [
				'first_studio'  => $_POST['studio1'],
				'first_phone'   => $_POST['phone1'],
				'second_studio' => $_POST['studio2'],
				'second_phone'  => $_POST['phone2'],
			];

			$this->my_query->insert_for_id($this->tbl , ['id' => 1] , $data);


			$this->session->set_flashdata('result' , 'info');
  			$this->session->set_flashdata('result_message' , 'Data Studio berhasil di rubah!' );
	    	redirect( base_url('Studio') );

		}


	}

	?>