<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	public function __construct() {
        parent::__construct();

        $this->load->model('my_query'); 
	}

	private $tbl = "user";

	public function index(){
		$this->session->sess_destroy();
		$this->load->view('page_login');

	}

	public function do_login(){

		$email    =	$_POST['email'];
		$password = $_POST['password'];

		$select = "*";
	
		$where 	=	[
			'user_email'    => $email,
			'password' =>  md5($password),
		];
		$result 	=	$this->my_query->get_data($select , $this->tbl  , $where )->result();
		
		if (count($result) > 0) {
			
			foreach ($result as $val) {
			
				$session['user_id']    = $val->user_id;
				$session['user_name']  = $val->user_name;
				$session['user_email'] = $val->user_email;
				$this->session->set_userdata($session);
				$this->session->set_flashdata('msg', 'Selamat datang '.$val->user_name);
				redirect('Dashboard');
			}

		} else {

			$this->session->set_flashdata('msg_type' , 'errror');
			$this->session->set_flashdata('msg' , 'Username / Password salah , silahkan coba kembali');
			redirect('Auth');

		}
	}
	function do_logout(){
		$this->session->sess_destroy();
		redirect( base_url('Auth/Index') );

	
	}
}
 ?>