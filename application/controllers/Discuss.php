<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Discuss extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('my_query');
	}

	private $tbl = 'post_discuss';

	function index(){
		$update_read_announce = $this->my_query->insert_for_id('announce' , ['to_user_id' => $this->session->userdata('user_id') ] , ['is_read' => 1] );
		
		$isi = [
			'post' 	=>	$this->my_query->get_data('*' , 'post' , ['user_id' => $this->session->userdata('user_id') ] )->result(),

		];

		$data= [
			'content' => $this->load->view('backend/discuss/discuss_index' , $isi , true)
		];

		$this->load->view('backend/content_dashboard', $data);		
	}
	function list_topics($post_id){
		$isi = [
			'discuss' => $this->my_query->get_data('*' , $this->tbl , ['post_id' => $post_id ] )->result()
		];

		$data= [
			'content' => $this->load->view('backend/discuss/discuss_detail' , $isi , true)
		];

		$this->load->view('backend/content_dashboard', $data);		
	}

	function jawab_pertanyaan($discuss_id , $post_id){	


		$data = [
			'post_discuss_id'=> $discuss_id,
			'text_reply'=> $_POST['reply_an'],
			'user_id' => $this->session->userdata('user_id')
 		];
		$this->my_query->insert_for_id('post_discuss_reply' , null , $data );

		$data_notification = [
			'messages'         => 'Admin Menjawab Pertanyaan diskusi anda!',
			'to_user_id'       => $_POST['to_user_id'],
			'from_user_id'     => $this->session->userdata('user_id'),
			'discuss_id'       => $discuss_id ,
			'post_id'          => null,
			'type'             => 'discuss_reply'
		];

		$discuss = $this->my_query->insert_for_id('announce' , null , $data_notification);
		
		$this->session->set_flashdata('result' , 'info');
  		$this->session->set_flashdata('result_message' , 'Pertanyaan berhasil di jawab');
		redirect( base_url('Discuss/list_topics/'.$post_id) );

	}

	function process($id = ""){

		$data = [
			'answer'   => $_POST['a'] ,
			'question' => $_POST['q'] ,
		];
		if ($id > 0) {

			$where = ['faq_id' => $id];
			$this->my_query->insert_for_id($this->tbl , $where , $data);

			$this->session->set_flashdata('result' , 'success');
  			$this->session->set_flashdata('result_message' , 'Faq Berhasil di update');
			
		} else {

			$this->my_query->insert_for_id($this->tbl , null , $data);

			$this->session->set_flashdata('result', 'success');
  			$this->session->set_flashdata('result_message', 'Faq Berhasil di tambahkan');
		}
		redirect( base_url('Faq') );
	}

	
}


