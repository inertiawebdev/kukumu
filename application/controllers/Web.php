<?php

class Web extends CI_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('my_query');
		$this->load->helper('indodate');
		ini_set('display_errors', 0);
	}

	// ================ PAGE INDEX ======================

	function index(){
		$this->load->view('frontend/index' );	
	}

	// ================ PAGE INDEX ======================





	
	// ================ PAGE HOME ======================

	function home(){

		$data = [
			'home' => $this->my_query->get_data('*', 'home' ,['home_id' => 1])->row(),
		];

		$this->load->view('frontend/home' , $data );	
	}

	// ================ PAGE HOME ======================

	// ================ PAGE STORY ======================

	function story(){

		$data = [
			'story'  => $this->my_query->get_data('*', 'stories_header' ,['id' => 1])->row(),
			'vismis' => $this->my_query->get_data('*', 'stories_vision' ,['id' => 1])->row(),
			'text'   => $this->my_query->get_data('*', 'stories_typography' ,['id' => 1])->row(),
		];

		$this->load->view('frontend/stories', $data );	
	}

	// ================ PAGE STORY ======================

	// ================ PAGE STUDIO ======================

	function studio(){
		$data = [
			'addr' => $this->my_query->get_data('*', 'studio' ,['id' => 1])->row(),
		];


		$this->load->view('frontend/studio' , $data );	
	}

	// ================ PAGE STUDIO ======================

	// ================ PAGE KELASKU ======================

	function kelas(){
		$data = [
			'kelas' => $this->my_query->get_data('*', 'kelasku' ,['id' => 1])->row(),
		];
		$this->load->view('frontend/kelasku' , $data );	
	}

	// ================ PAGE KELASKU ======================

	// ================ PAGE KEPERLUANKU ======================

	function kerperluan(){
		$data = [
			'intro' => $this->my_query->get_data('*', 'keperluanku_intro' ,['keperluanku_intro_id' => 1])->row(),
			'head' => $this->my_query->get_data('*', 'keperluanku_header' ,['id' => 1])->row(),
			'leaf' => $this->my_query->get_data('*', 'leaeaf' ,['id' => 1])->row(),
		];
		$this->load->view('frontend/keperluanku' ,$data);	
	}

	// ================ PAGE KEPERLUANKU ======================

	// ================ PAGE CAMPAIGN ======================

	function campaign(){

		$data = [
			'header' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 1 ])->row(), // header
			'video' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 10 ])->row(),
			'pic1' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 2 ])->row(),
			'pic2' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 3 ])->row(),
			'pic3' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 4 ])->row(),
			'pic4' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 5 ])->row(),
			'pic5' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 6 ])->row(),
			'pic6' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 7 ])->row(),
			'pic7' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 8 ])->row(),
			'pic8' => $this->my_query->get_data('*'  , 'campaign' ,  ['id' => 9 ])->row(),
		];


		$this->load->view('frontend/campaign' , $data );	
	}

	// ================ PAGE CAMPAIGN ======================




	// COUNTING VISITOR

	function getuserIP(){
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
	        //ip from share internet
	        $ip = $_SERVER['HTTP_CLIENT_IP'];
	    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	        //ip pass from proxy
	        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    }else{
	        $ip = $_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}

	function count_visit(){
		
		$ip        = $this->getuserIP();
		$date      = date('Y-m-d');
		$useragent = $_SERVER['HTTP_USER_AGENT'];

		$wh = ['visitor_ip' => $ip , 'date' => $date];

		$check = $this->my_query->get_data('*' , 'visitors', $wh);

		if ($check->num_rows() > 0) {
			
			$wh['visitor_user_agent'] = $useragent;

			$check2 = $this->my_query->get_data('*' , 'visitors', $wh);

			if ($check2->num_rows() >0) {

			} else {

				$data = [
					'visitor_ip' => $ip,
					'visitor_user_agent' => $useragent,
					'date' => $date
				];		
				$this->my_query->insert_for_id('visitors' , null , $data);
			}
		} else {
			$data = [
				'visitor_ip' => $ip,
				'visitor_user_agent' => $useragent,
				'date' => $date
			];		
			$this->my_query->insert_for_id('visitors' , null , $data);
		}
	}


	// ============ Do Login ===============
	


	//MESSAGES -==================================
}