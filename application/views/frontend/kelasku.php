<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php $this->load->view('frontend/kukumu_header'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- App Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/css/app.min.css">
    <title>Kukuku Kukumu</title>
</head>

<body>
    <?php $this->load->view('frontend/kukumu_tagmanager'); ?>
    <!-- Navbar -->
    <?php $this->load->view('frontend/kukuku_menu') ?>
    <!-- End Navbar -->

    <!-- Content -->

    <!-- Row 1  -->
    <section class="row kelasku-row-1 new-section" style="background-image: url(<?= base_url() ?>assets/frontend/assets/images/kelasku/bg-1.png);">
        <div class="col-sm-12 col-lg-4 d-flex justify-content-center align-items-center">
            <h1>Get to know</h1>
        </div>
        <div class="col-sm-12 col-lg-4 d-flex justify-content-center align-items-center">
            <img src="<?= base_url() ?>assets/frontend/assets/images/kelasku/kelasku-logo.png" alt="kelas kukuku" width="150">
        </div>
        <div class="col-sm-12 col-lg-4 d-flex justify-content-center align-items-center">
            <h2>KELASKU</h2>
        </div>
    </section>
    <!-- End Row 1 -->

    <!-- Row 2 -->

    <section class="row kelasku-row-2 new-section slide-left">
        <div class="col-sm-12 col-lg-6 d-flex flex-column justify-content-center align-items-center" style="background-image: url(<?= base_url() ?>assets/frontend/assets/images/kelasku/bg-2.png);">
           <?php
                $first = $kelas->description ;
                $first = str_replace(',', '<span class="text-twc">,</span>', $first);
                $first = str_replace('.', '<span class="text-twc">.</span>', $first);

                echo $first;
            ?>
        </div>
        <div class="col-sm-12 col-lg-6 img-slide" style="background-image: url(<?= base_url() ?>assets/img/<?= $kelas->picture ?>);"></div>
    </section>

    <!-- End Row 2 -->

    <!-- End Content -->

    <!-- Footer -->
    <?php

        $this->load->view('frontend/kukumu_footer');

     ?>
    <!-- End Footer -->

    <!-- Jquery Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Mousewheel -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <!-- App Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/js/app.min.js"></script>
</body>

</html>
