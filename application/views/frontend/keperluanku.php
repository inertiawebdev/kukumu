<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php $this->load->view('frontend/kukumu_header'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name='keywords' content='kukuku, kukumu, riasan kuku, awet, terjamin' />
    <meta name="desription" content="The only place to get your nails a handful of love and care, KUKUKU KUKUMU provide: nail art and services; workshop or classes; all the tools you need for hobby and business, is ready to serve.">
    
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="kukuku kukumu"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-310x310.png" />

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="Kukuku Kukumu">
    <meta itemprop="description" content="The only place to get your nails a handful of love and care">
    <meta itemprop="image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    
    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://kukumu.com">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Kukuku Kukumu">
    <meta property="og:description" content="The only place to get your nails a handful of love and care">
    <meta property="og:image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    
    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Kukuku Kukumu">
    <meta name="twitter:description" content="The only place to get your nails a handful of love and care">
    <meta name="twitter:image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    <!-- Bootstrap Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- App Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/css/app.min.css">
    <title>Kukuku Kukumu</title>
</head>

<body>
    <?php $this->load->view('frontend/kukumu_tagmanager'); ?>
    <!-- Navbar -->
    <?php $this->load->view('frontend/kukuku_menu') ?>
    <!-- End Navbar -->

    <!-- Content -->

    <!-- Row 1  -->
    <section class="row keperluanku-row-1 new-section" style="background-image: url(<?= base_url() ?>assets/frontend/assets/images/keperluanku/bg-1.png);">
        <div class="col-sm-12 col-lg-4 d-flex flex-column justify-content-center column-1">
            <p>
                <?php
                    $first = $intro->desc1 ;
                    $first = str_replace(',', '<span class="text-twc">,</span>', $first);
                    $first = str_replace('.', '<span class="text-twc">.</span>', $first);

                    echo $first;
                ?>
            </p>
        </div>
        <div class="col-sm-12 col-lg-4 d-flex flex-column align-items-center justify-content-center column-2">
            <div>
                <img src="<?= base_url() ?>assets/frontend/assets/images/keperluanku/logo-word.png" alt="keperluanku kukukukukumu" width="210">
            </div>
            <div class="mt-n5">
                <img src="<?= base_url() ?>assets/frontend/assets/images/keperluanku/logo-icon.png" alt="keperluanku kukukukukumu" width="240">
            </div>
        </div>
        <div class="col-sm-12 col-lg-4 d-flex flex-column justify-content-center column-3">
            <p>
               <?php
                    $sec = $intro->desc2 ;
                    $sec = str_replace(',', '<span class="text-twc">,</span>', $sec);
                    $sec = str_replace('.', '<span class="text-twc">.</span>', $sec);

                    echo $sec;
                ?>
            </p>
            <a href="#continue-content" id="continue-read">Read more</a>
        </div>
    </section>
    <!-- End Row 1  -->

    <!-- Row 2 -->
    <section class="row keperluanku-row-2 new-section overflow-hidden" id="continue-content">
        <div class="col-sm-12 col-lg-6 column-1">
            <?php
                    $desc = $head->description ;
                    $desc = str_replace(',', '<span class="text-twc">,</span>', $desc);
                    $desc = str_replace('.', '<span class="text-twc">.</span>', $desc);

                    echo $desc;
            ?>

        </div>
        <div class="col-sm-12 col-lg-6 overflow-hidden p-0">
            <img src="<?= base_url() ?>assets/img/<?= $head->picture ?>" alt="kukukukukumu" class="w-100">
        </div>
    </section>
    <!-- End Row 2 -->

    <!-- Row 3 -->
    <section class="row keperluanku-row-3 new-section">
        <div class="col-12 d-flex flex-column">
            <div class="title">
                <h2 class="text-white">Leaf Gel</h2>
                <?php
                        $leaf1 = $leaf->description ;
                        $leaf1 = str_replace(',', '<span class="text-twc">,</span>', $leaf1);
                        $leaf1 = str_replace('.', '<span class="text-twc">.</span>', $leaf1);

                        echo $leaf1;
                ?>
                
            </div>
            <div class="list">
                <div class="list-content">
                    <span class="avenir-black">1.</span><br><?= $leaf->desc_1 ?>
                </div>
                <div class="list-content">
                    <span class="avenir-black">2.</span><br><?= $leaf->desc_2 ?>
                </div>
                <div class="list-content">
                    <span class="avenir-black">3.</span><br><?= $leaf->desc_3 ?>
                </div>
                <div class="list-content">
                    <span class="avenir-black">4.</span><br><?= $leaf->desc_4 ?>
                </div>
            </div>
        </div>
    </section>
    <!-- End Row 3 -->

    <section class="row keperluanku-row-4">
        <div class="col-12 d-flex flex-column">
            <div class="title">
                <h2 class="text-white">CND</h2>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <p>
                            We are proud to announce ourselves as the Exclusive Partner of CND (Creative Nail Design,
                            Inc<span class="text-twc">.</span>) the global leader in professional nail, hand and foot
                            beauty<span class="text-twc">.</span> Founded in 1979, this California-based industry is deeply committed to advancing the role of nail care in personal
                            beauty and fashion<span class="text-twc">.</span>
                        </p>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <p>
                            One of CND products available at Kukuku Kukumu are the CND Spa Collection, a wide-ranging
                            selection of exceptional spa treatments which work wonders for your skin<span
                                class="text-twc">.</span> From hydrating, softening, brightening, soothing—you name it,
                            we got it<span class="text-twc">.</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End Content -->

    <!-- Footer -->
    <?php

        $this->load->view('frontend/kukumu_footer');

     ?>
    <!-- End Footer -->

    <!-- Jquery Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Mousewheel -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <!-- App Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/js/app.js"></script>
</body>

</html>
