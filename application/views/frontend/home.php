<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php $this->load->view('frontend/kukumu_header'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name='keywords' content='kukuku, kukumu, riasan kuku, awet, terjamin' />
    <meta name="desription" content="The only place to get your nails a handful of love and care, KUKUKU KUKUMU provide: nail art and services; workshop or classes; all the tools you need for hobby and business, is ready to serve.">
    
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="kukumu"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-310x310.png" />

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="Kukuku Kukumu">
    <meta itemprop="description" content="The only place to get your nails a handful of love and care">
    <meta itemprop="image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    
    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://kukumu.com">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Kukuku Kukumu">
    <meta property="og:description" content="The only place to get your nails a handful of love and care">
    <meta property="og:image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    
    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Kukuku Kukumu">
    <meta name="twitter:description" content="The only place to get your nails a handful of love and care">
    <meta name="twitter:image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    <!-- Bootstrap Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- App Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/css/app.css">
    <title>Kukuku Kukumu</title>
</head>
<body>
    <?php $this->load->view('frontend/kukumu_tagmanager'); ?>
    <!-- Navbar -->
    <?php $this->load->view('frontend/kukuku_menu') ?>
    <!-- End Navbar -->

    <!-- Content -->

    <section>
        <div class="row m-0 home-intro overflow-hidden" style="background-image: url('<?= base_url() ?>assets/frontend/assets/images/bg-home.png');">
            <div class="col-12 d-flex home-intro-column align-items-center justify-content-center p-0">
                <span class="text-white mt-2">こ ん に ち は</span>
                <div class="row">
                    <div class="col-sm-12 col-lg-6 text-white">
                        <div class="mx-auto home-intro-left">
                            <!-- <h1 class="mb-3">Welcome to</h1> -->
                            <!-- <h1>KUKUKU KUKUMU!</h1> -->
                            <h1><?= $home->headline ?></h1>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6 mx-auto">
                        <div class="mx-auto home-intro-right">
                            <!-- <p class="mb-5">
                                The only place to get your nails a handful of love and care, KUKUKU KUKUMU is a 'one place - serves
                                all' for your nails necessities, We provide: nail art and care services<text
                                    class="text-twc">;</text> workshop or classes<text class="text-twc">;</text> all of
                                the tools you need for your nail art hobby and business, KUKUKU KUKUMU is ready to serve you!
                            </p>
                            <p class="mt-5">
                                But first<text class="text-arial">,</text> <a href="stories.html">Let's hold hands,</a>
                            </p> -->
                            <?= $home->description ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Content -->

    <!-- Footer -->
    <?php

        $this->load->view('frontend/kukumu_footer');

     ?>
    <!-- End Footer -->

    <!-- Jquery Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Mousewheel -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <!-- App Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/js/app.js"></script>
</body>
</html>
