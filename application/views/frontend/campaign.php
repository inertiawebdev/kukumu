<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php $this->load->view('frontend/kukumu_header'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name='keywords' content='kukuku, kukumu, riasan kuku, awet, terjamin' />
    <meta name="desription" content="The only place to get your nails a handful of love and care, KUKUKU KUKUMU provide: nail art and services; workshop or classes; all the tools you need for hobby and business, is ready to serve.">
    
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="kukumu"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-310x310.png" />

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="Kukuku Kukumu">
    <meta itemprop="description" content="The only place to get your nails a handful of love and care">
    <meta itemprop="image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    
    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://kukumu.com">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Kukuku Kukumu">
    <meta property="og:description" content="The only place to get your nails a handful of love and care">
    <meta property="og:image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    
    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Kukuku Kukumu">
    <meta name="twitter:description" content="The only place to get your nails a handful of love and care">
    <meta name="twitter:image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">

    <!-- Bootstrap Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- App Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/css/app.min.css">
    <title>Kukumu | Ordinary Pleasure</title>
</head>

<body>
    <?php $this->load->view('frontend/kukumu_tagmanager'); ?>
    <!-- Navbar -->
    <?php $this->load->view('frontend/kukuku_menu') ?>
    <!-- End Navbar -->

    <!-- Content -->
    <!-- Row 1 -->
    <section class="row m-0 new-section">
        <div class="col-12 d-flex justify-content-center align-items-center campaign-column-1" style="background-image: url(<?= base_url() ?>assets/img/<?= $header->file ?>);">
            <img src="<?= base_url() ?>assets/frontend/assets/images/campaign/logo.png" alt="kukukukukumu" width="150">
        </div>
    </section>
    <!-- Row 2 -->

    <!-- Video -->

    <div class="row m-0 my-2">
        <div class="col-lg-8 mx-auto p-0">
            <video class="w-100 video-ordinary" controls poster="data:image/gif,AAAA" style="background-image: url(<?= base_url() ?>assets/frontend/assets/images/campaign/bg.png);">
                <source src="<?= base_url() ?>assets/video/<?= $video->file ?>" type="video/mp4" />
            </video>
            <!-- <div class="column-embed-video">
                <iframe class="w-100 h-100 embed-video" src="https://www.youtube.com/embed/pE5967je4lg" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            </div> -->
        </div>
    </div>

    <!-- End Video -->

    <!-- Gallery -->
    <section class="row m-0">
        <div class="col-12 campaign-gallery-1">
            <img src="<?= base_url() ?>assets/img/<?= $pic1->file ?>" alt="kukukukukumu" class="w-100">
        </div>
        <div class="col-12 campaign-gallery-2">
            <img src="<?= base_url() ?>assets/img/<?= $pic2->file ?>" alt="kukukukukumu" class="w-100">
        </div>
        <div class="col-12 campaign-gallery-3">
            <img src="<?= base_url() ?>assets/img/<?= $pic3->file ?>" alt="kukukukukumu" class="w-100">
        </div>
        <div class="col-12 campaign-gallery-4">
            <img src="<?= base_url() ?>assets/img/<?= $pic4->file ?>" alt="kukukukukumu" class="w-100">
        </div>
        <div class="col-12 campaign-gallery-5">
            <img src="<?= base_url() ?>assets/img/<?= $pic5->file ?>" alt="kukukukukumu" class="w-100">
        </div>
        <div class="col-12 campaign-group p-0">
            <div class="row m-0">
                <div class="col-sm-4 campaign-gallery-6">
                    <img src="<?= base_url() ?>assets/img/<?= $pic6->file ?>" alt="kukukukukumu" class="w-100">
                </div>
                <div class="col-sm-4 campaign-gallery-7">
                    <img src="<?= base_url() ?>assets/img/<?= $pic7->file ?>" alt="kukukukukumu" class="w-100">
                </div>
                <div class="col-sm-4 campaign-gallery-8">
                    <img src="<?= base_url() ?>assets/img/<?= $pic8->file ?>" alt="kukukukukumu" class="w-100">
                </div>
            </div>
        </div>
    </section>
    <!-- End Gallery -->
    <!-- End Content -->

    <!-- Footer -->
    <?php

        $this->load->view('frontend/kukumu_footer');

     ?>
    <!-- End Footer -->

    <!-- Jquery Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- App Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/js/app.min.js"></script>
</body>

</html>
