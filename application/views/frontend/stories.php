<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php $this->load->view('frontend/kukumu_header'); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name='keywords' content='kukuku, kukumu, riasan kuku, awet, terjamin' />
    <meta name="desription" content="The only place to get your nails a handful of love and care, KUKUKU KUKUMU provide: nail art and services; workshop or classes; all the tools you need for hobby and business, is ready to serve.">
    
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?= base_url() ?>assets/frontend/assets/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/frontend/assets/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="kukumu"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="<?= base_url() ?>assets/frontend/assets/favicon/mstile-310x310.png" />

    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="Kukuku Kukumu">
    <meta itemprop="description" content="The only place to get your nails a handful of love and care">
    <meta itemprop="image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    
    <!-- Facebook Meta Tags -->
    <meta property="og:url" content="https://kukumu.com">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Kukuku Kukumu">
    <meta property="og:description" content="The only place to get your nails a handful of love and care">
    <meta property="og:image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    
    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Kukuku Kukumu">
    <meta name="twitter:description" content="The only place to get your nails a handful of love and care">
    <meta name="twitter:image" content="<?= base_url() ?>assets/frontend/assets/images/index/logo.png">
    <!-- Bootstrap Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/css/bootstrap.min.css">
    <!-- App Css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/frontend/assets/css/app.min.css">
    <title>Kukuku Kukumu</title>
</head>

<body>
    <?php $this->load->view('frontend/kukumu_tagmanager'); ?>
    <!-- Navbar -->
    <?php $this->load->view('frontend/kukuku_menu') ?>
    <!-- End Navbar -->

    <!-- Content -->
    <section class="row stories-page" style="background-image: url('<?= base_url() ?>assets/frontend/assets/images/stories/bg-1.png')">
        <div class="col-12 p-0">
            <!-- Row 1 -->
            <div class="row m-0 new-section">
                <div class="col-sm-12 col-lg-6 stories-column-1 d-flex justify-content-center flex-column mt-5">
                    <?= $story->story_desc ?>
                    <a href="#continue-content" id="continue-read" class="mt-3 text-white">Read More</a>
                </div>
                <div class="col-sm-12 col-lg-6 stories-column-2" style="background-image: url(<?= base_url() ?>assets/img/<?= $story->story_picture ?>);">
                </div>
            </div>
            <!-- End Row 1 -->
            
            <!-- Row 2 -->
            <div class="row m-0 new-section" id="continue-content">
                <div class="col-sm-12 col-lg-5 stories-column-3">
                   <?php
                        $first = $story->story_desc_first_paragraph ;
                        $first = str_replace(',', '<span class="text-twc">,</span>', $first);
                        $first = str_replace('.', '<span class="text-twc">.</span>', $first);

                        echo $first;
                   ?>


                   <!--  <p>
                        But to Evolin Patanmacia dismay<span class="text-twc">,</span> there was no enjoyable nail care
                        studio that can offer a good nail
                        care
                        treatment to cater to her needs when she returned to Pekanbaru from Australia<span class="text-twc">.</span>
                        She saw the void and
                        decided to open her own Nail Studio called KUKUKU in 2011<span class="text-twc">.</span> In
                        Indonesian<span class="text-twc">,</span> KUKUKU literally means "my
                        nails"<span class="text-twc">.</span>
                    </p>
                    <p>
                        As KUKUKU has entered its eighth year in the business, our nail studio has evolved into KUKUKU
                        KUKUMU<span class="text-twc">.</span>
                        KUKUMU literally means "your nails"<span class="text-twc">.</span>
                        It is an additional segment to our service dedicated to male customers who can have a nice hot stone
                        massage as they wait for their wives or girlfriends to get their nails done<span class="text-twc">.</span>
                        We merged both KUKUKU
                        and
                        KUKUMU as a flagship branch and name the upcoming branches as KUKUKU KUKUMU throughout
                        Indonesia<span class="text-twc">.</span>
                    </p> -->
                </div>
                <div class="col-sm-12 col-lg-5 stories-column-4">
                    <?php
                        $second = $story->story_desc_second_paragraph ;
                        $second = str_replace(',', '<span class="text-twc">,</span>', $second);
                        $second = str_replace('.', '<span class="text-twc">.</span>', $second);

                        echo $second;
                   ?>
                </div>
            </div>
            
            <!-- End Row 2 -->
            
            <!-- Row 3 -->
            <div class="row m-0 new-section">
                <div class="col-12 stories-row" style="background-color: #FEEFBA;">
                    <div class="position-absolute" style="right: 10%;margin-top: 7%;">
                        <img src="<?= base_url() ?>assets/frontend/assets/images/stories/logo.png" alt="kukukukukumu" class="circle-quote">
                    </div>
                    <div class="row" style="margin-top: 80px;">
                        <div class="col-12 stories-column-5">
                            <div class="circle-text-column-2">
                                <span class="circle-text" id="circle-word-1">V</span>
                                <span class="circle-text" id="circle-word-2">I</span>
                                <span class="circle-text" id="circle-word-3">S</span>
                                <span class="circle-text" id="circle-word-4">I</span>
                                <span class="circle-text" id="circle-word-5">O</span>
                                <span class="circle-text" id="circle-word-6" style="margin-left:6px;margin-top: 5px;">N</span>
                            </div>
                            <p>
                               <?php
                                $vis1 = $vismis->vision_paragraph_1;
                                $vis1 = str_replace(',', '<span class="text-twc">,</span>', $vis1);
                                $vis1 = str_replace('.', '<span class="text-twc">.</span>', $vis1);

                                echo $vis1;

                               ?>

                            </p>
                        </div>
                        <div class="col-12 stories-column-5">
                            <?php
                                $vis2 = $vismis->vision_paragraph_2;
                                $vis2 = str_replace(',', '<span class="text-twc">,</span>', $vis2);
                                $vis2 = str_replace('.', '<span class="text-twc">.</span>', $vis2);

                                echo $vis2;

                               ?>
                        </div>
                        <div class="col-12 stories-column-5">
                            <?php
                                $vis3 = $vismis->vision_paragraph_3;
                                $vis3 = str_replace(',', '<span class="text-twc">,</span>', $vis3);
                                $vis3 = str_replace('.', '<span class="text-twc">.</span>', $vis3);

                                echo $vis3;

                               ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 stories-column-5">
                            <div class="circle-text-column-2">
                                <span class="circle-text" id="circle-word-1">M</span>
                                <span class="circle-text" id="circle-word-2">I</span>
                                <span class="circle-text" id="circle-word-3">S</span>
                                <span class="circle-text" id="circle-word-4">S</span>
                                <span class="circle-text" id="circle-word-5">I</span>
                                <span class="circle-text" id="circle-word-6">O</span>
                                <span class="circle-text" id="circle-word-7">N</span>
                            </div>
                            <p class="sub-text-circle">
                               <?php
                                $mis1 = $vismis->mission_paragraph_1;
                                $mis1 = str_replace(',', '<span class="text-twc">,</span>', $mis1);
                                $mis1 = str_replace('.', '<span class="text-twc">.</span>', $mis1);

                                echo $mis1;

                                ?>
                            </p>
                        </div>
                        <div class="col-12 stories-column-5">
                            <p class="sub-text-circle">
                            <?php

                            $mis2 = $vismis->mission_paragraph_2;
                            $mis2 = str_replace(',', '<span class="text-twc">,</span>', $mis2);
                            $mis2 = str_replace('.', '<span class="text-twc">.</span>', $mis2);

                            echo $mis2;

                            ?>
                            </p>
                        </div>
                        <div class="col-12 stories-column-5">
                            <p class="sub-text-circle">
                            <?php
                                $mis3 = $vismis->mission_paragraph_3;
                                $mis3 = str_replace(',', '<span class="text-twc">,</span>', $mis3);
                                $mis3 = str_replace('.', '<span class="text-twc">.</span>', $mis3);

                                echo $mis3;

                             ?>
                         </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row 3 -->
            
            <!-- Row 4 -->
            <div class="row m-0 new-section">
                <div class="col-12 stories-final-row d-flex align-items-end">
                    
                        <?= $text->stories_desc ?>
                </div>
            </div>
            <!-- End Row 4 -->
        </div>
    </section>
    <!-- End Content -->

    <!-- Footer -->
    <?php

        $this->load->view('frontend/kukumu_footer');

     ?>
    <!-- End Footer -->
    
    <!-- Jquery Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Mousewheel -->
    <script src="<?= base_url() ?>assets/frontend/assets/vendor/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <!-- App Js -->
    <script src="<?= base_url() ?>assets/frontend/assets/js/app.js"></script>
</body>

</html>
