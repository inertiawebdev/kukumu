<?php
    
    $data = $this->my_query->get_data('*' , 'footer' )->row();

 ?>


<footer class="row m-0 footer-app overflow-hidden">
    <div class="col-sm-12 col-md-6 col-lg-6 overflow-hidden d-flex align-items-end">
        <a href="<?= $data->ig_link ?>" target="_blank" class="footer-sosmed"><img
                src="<?= base_url() ?>assets/frontend/assets/icons/ig.png" alt="kukumu-facebook" class="footer-icon"></a>
        <a href="<?= $data->tokped_link ?>" target="_blank" class="footer-sosmed"><img
                src="<?= base_url() ?>assets/frontend/assets/icons/bag.png" alt="kukumu-tokopedia" width="20"></a>
    </div>
    <div class="col-sm-12 contact-column col-md-6 col-lg-6 overflow-hidden d-flex flex-column justify-content-end">
        <p class="footer-title">Contact Us</p>
        <p><?= $data->website ?></p>
        <p class="mb-0"><?= $data->mail ?></p>
    </div>
</footer>
<link rel="stylesheet" href="<?=base_url()?>assets/frontend/assets/css/inertia.css">
