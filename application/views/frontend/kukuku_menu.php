<nav class="navbar navbar-expand-lg navbar-light fixed-top m-0">
    <a class="navbar-brand" href="<?= base_url() ?>Homeku"><img src="<?= base_url() ?>assets/frontend/assets/images/index/logo.png" alt="kukumu" class="navbar-logo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
        aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mx-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link text-white" href="<?= base_url() ?>Stories">Stories</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="<?= base_url() ?>Studioku">Studio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="<?= base_url() ?>Kelasku">Kelasku</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white" href="<?= base_url() ?>Keperluanku">Keperluanku</a>
            </li>
        </ul>
        <div class="mr-4" id="search-column">
            <a href="#" id="search-btn">
                <img src="<?= base_url() ?>assets/frontend/assets/icons/search.png" alt="search-kukumu" width="20">
            </a>
        </div>
        <form class="form-inline my-2 my-lg-0 hide" id="search-form">
            <input class="form-control mr-sm-2 rounded-0" type="search" placeholder="Search" autofocus>
        </form>
        <div class="btn-group">
            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"
                data-display="static" aria-haspopup="true" aria-expanded="false">
                <img src="<?= base_url() ?>assets/frontend/assets/icons/menu.png" alt="menu-kukumu" width="25"> 
            </button>
            <div class="dropdown-menu dropdown-menu-lg-right rounded-0">
                <a href="#" class="dropdown-item text-white text-center">Constellation
                    House</a>
                <a href="<?= base_url() ?>Ordinary" class="dropdown-item text-white text-center pr-5">Ordinary Pleasure</a>
            </div>
        </div>
    </div>
</nav>