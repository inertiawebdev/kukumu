<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <title>AdminBite admin Template - The Ultimate Multipurpose admin template</title>
    <!-- Custom CSS -->
    <link href="<?= base_url() ?>assets/backend/assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/backend/assets/extra-libs/c3/c3.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/backend/assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/backend/assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/libs/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/assets/libs/ckeditor/samples/css/samples.css">
    <!-- Custom CSS -->
    <link href="<?= base_url() ?>assets/backend/dist/css/style.min.css" rel="stylesheet">
    <script src="<?= base_url() ?>assets/backend/assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/backend/assets/libs/toastr/build/toastr.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<?php   
  $session = $this->session->flashdata('result');
  $msg     = $this->session->flashdata('result_message');
?>
<script type="text/javascript">
    $(document).ready(function(){
        var ses= "<?= $session ?>";

        if (ses != "") {
          toastr[ses]('<?= $msg ?>');
        }
    });
</script>
</head>