<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Stories Head </h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Stories Head </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
      
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title">Upload Video Play</h4> -->
                    <h6 class="card-subtitle">  </h6>
                    <form class="m-t-30" action="#" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <button  type="button" onclick="window.location.href='<?= base_url() ?>Storieshead/index/edit' " id="btn_edit" class="btn btn-success">
                                <i class="fa fa-edit"></i> Edit 
                            </button>
                           
                        </div>

                        <div class="form-group">
                            <label>Description Intro</label>
                            <textarea name="desc" readonly class="form-control ckeditor" ><?= $story->story_desc ?></textarea>
                        </div>

                        <div class="form-group">
                            <label >First Description Paragraph</label>
                            <textarea name="desc1" readonly class="form-control ckeditor" ><?= $story->story_desc_first_paragraph ?></textarea>
                        </div>

                        <div class="form-group">
                            <label >Second Description Paragraph</label>
                            <textarea name="desc2" readonly class="form-control ckeditor" ><?= $story->story_desc_second_paragraph ?></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label >Input Picture</label>
                            <input type="file" readonly name="descpicture" class="form-control" >
                            <small class="text-danger">Ukuran ideal  3 x 4 (Maks height: 1200 x width : 1000 ) </small>
                        </div>

                        <div class="form-group">
                            <label >Preview Picture</label><br>
                            <img style="height: 400px;width: 300px;" src="<?= base_url() ?>assets/img/<?= $story->story_picture ?>">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  