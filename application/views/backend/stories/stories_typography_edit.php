<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Stories Typography </h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Stories Vision & Mission </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
      
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title">Typography</h4> -->
                    <h6 class="card-subtitle">  </h6>
                    <form class="m-t-30" action="<?= base_url() ?>Typhography/update" method="post" >
                        <div class="form-group">
                            <button  type="button" onclick="window.location.href='<?= base_url() ?>Typhography/index/'" id="btn_edit_vis" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                           
                        </div>

                        <div class="form-group">
                            <label>Paragraph 1</label>
                            <textarea rows="6" name="stories_desc"   class="form-control ckeditor" ><?= $typo->stories_desc ?></textarea>
                        </div>

                       
                        <button  style="display:;" id="btn_save_vis" class="btn btn-info" type="submit">
                            <i class="fas fa-save"></i> Simpan Perubahan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    
</div>  

<script type="text/javascript">
    
</script>