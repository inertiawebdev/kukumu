<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Stories Vision & Mission </h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Stories Vision & Mission </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
      
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Vision</h4>
                    <h6 class="card-subtitle">  </h6>
                    <form class="m-t-30" action="<?= base_url() ?>Vision/update/vision" method="post" >
                        <div class="form-group">
                            <button  type="button" onclick="edit_vis()" id="btn_edit_vis" class="btn btn-success">
                                <i class="fa fa-edit"></i> Edit Vision
                            </button>
                            <button  type="button" onclick="cancel_vis()" id="btn_cancel_vis" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                           
                        </div>

                        <div class="form-group">
                            <label>Paragraph 1</label>
                            <textarea rows="6" name="vision_paragraph_1"   class="form-control vis" ><?= $vis->vision_paragraph_1 ?></textarea>
                        </div>

                        <div class="form-group">
                            <label >Paragraph 2</label>
                            <textarea rows="6" name="vision_paragraph_2"   class="form-control vis" ><?= $vis->vision_paragraph_2 ?></textarea>
                        </div>

                        <div class="form-group">
                            <label >Paragraph 3</label>
                            <textarea rows="6" name="vision_paragraph_3"   class="form-control vis" ><?= $vis->vision_paragraph_3 ?></textarea>
                        </div>
                        <button  style="display:none;" id="btn_save_vis" class="btn btn-info" type="submit">
                            <i class="fas fa-save"></i> Simpan Perubahan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Mission</h4>
                    <h6 class="card-subtitle">  </h6>
                    <form class="m-t-30" action="<?= base_url() ?>Vision/update/mission" method="post" >
                        <div class="form-group">
                            <button  type="button" onclick="edit_mis()" id="btn_edit_mis" class="btn btn-success">
                                <i class="fa fa-edit"></i> Edit Mission
                            </button>
                            <button  type="button" onclick="cancel_mis()" id="btn_cancel_mis" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                           
                        </div>

                       <div class="form-group">
                            <label>Paragraph 1</label>
                            <textarea rows="6" name="mission_paragraph_1"   class="form-control mis" ><?= $vis->mission_paragraph_1 ?></textarea>
                        </div>

                        <div class="form-group">
                            <label >Paragraph 2</label>
                            <textarea rows="6" name="mission_paragraph_2"   class="form-control mis" ><?= $vis->mission_paragraph_2 ?></textarea>
                        </div>

                        <div class="form-group">
                            <label >Paragraph 3</label>
                            <textarea rows="6" name="mission_paragraph_3"   class="form-control mis" ><?= $vis->mission_paragraph_3 ?></textarea>
                        </div>
                        
                        <button  style="display:none;" id="btn_save_mis" class="btn btn-info" type="submit">
                            <i class="fas fa-save"></i> Simpan Perubahan
                        </button>
                        

                        

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  

<script type="text/javascript">
    $(document).ready(function($) {
        
        $(".vis").prop('readonly' , true);
        $(".mis").prop('readonly' , true);
        $("#btn_cancel_vis").hide();
        $("#btn_cancel_mis").hide();
    });

    function edit_vis(){
        $(".vis").prop('readonly' , false);
        $("#btn_save_vis").show();
        $("#btn_cancel_vis").show();
        $("#btn_edit_vis").hide();
    }

     function cancel_vis(){
        $(".vis").prop('readonly' , true);
        $("#btn_save_vis").hide();
        $("#btn_cancel_vis").hide();
        $("#btn_edit_vis").show();
    }

    function edit_mis(){
        $(".mis").prop('readonly' , false);
        $("#btn_save_mis").show();
        $("#btn_cancel_mis").show();
        $("#btn_edit_mis").hide();
    }

     function cancel_mis(){
        $(".mis").prop('readonly' , true);
        $("#btn_save_mis").hide();
        $("#btn_cancel_mis").hide();
        $("#btn_edit_mis").show();
    }
</script>