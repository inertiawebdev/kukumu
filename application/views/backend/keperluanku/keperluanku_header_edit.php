    
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Keperluanku Center</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Keperluanku </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
      
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title">Upload Video Play</h4> -->
                    <h6 class="card-subtitle">  </h6>
                    <form class="m-t-30" action="<?= base_Url() ?>Needshead/update/" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <button  type="button" onclick="window.location.href='<?= base_url() ?>Myclass/index/' " id="btn_edit" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel 
                            </button>
                           
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label>Description</label>
                            </div>
                            <div class="col-md-9">
                                <textarea name="desc"  class="form-control ckeditor" id="ckeditor"><?= $keperluanku->description ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label>Gambar</label>
                            </div>
                            <div class="col-md-9">
                               <input type="file" name="name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title"></label>
                            <img src="<?= base_url() ?>assets/img/<?= $keperluanku->picture ?>" style="width:400px;height:500px;"> 
                        </div>
                        <button  type="submit" class="btn btn-info">
                            <i class="fa fa-save"></i> Simpan perubahan 
                        </button>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  