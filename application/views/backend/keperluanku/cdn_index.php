<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">CDN Keperluanku </h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> CDN Index </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
      
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    
                    <form class="m-t-30" action="<?= base_url() ?>Cdn/update/" method="post" >
                        <div class="form-group">
                            <button  type="button" onclick="edit_vis()" id="btn_edit_vis" class="btn btn-success">
                                <i class="fa fa-edit"></i> Edit
                            </button>
                            <button  type="button" onclick="cancel_vis()" id="btn_cancel_vis" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                           
                        </div>

                        <div class="form-group">
                            <label>Paragraph 1</label>
                            <textarea rows="6" name="text1"   class="form-control vis" ><?= $cdn->text1 ?></textarea>
                        </div>

                        <div class="form-group">
                            <label >Paragraph 2</label>
                            <textarea rows="6" name="text2"   class="form-control vis" ><?= $cdn->text2 ?></textarea>
                        </div>

                       
                        <button  style="display:none;" id="btn_save_vis" class="btn btn-info" type="submit">
                            <i class="fas fa-save"></i> Simpan Perubahan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    
</div>  

<script type="text/javascript">
    $(document).ready(function($) {
        
        $(".vis").prop('readonly' , true);
        $(".mis").prop('readonly' , true);
        $("#btn_cancel_vis").hide();
        $("#btn_cancel_mis").hide();
    });

    function edit_vis(){
        $(".vis").prop('readonly' , false);
        $("#btn_save_vis").show();
        $("#btn_cancel_vis").show();
        $("#btn_edit_vis").hide();
    }

     function cancel_vis(){
        $(".vis").prop('readonly' , true);
        $("#btn_save_vis").hide();
        $("#btn_cancel_vis").hide();
        $("#btn_edit_vis").show();
    }

    function edit_mis(){
        $(".mis").prop('readonly' , false);
        $("#btn_save_mis").show();
        $("#btn_cancel_mis").show();
        $("#btn_edit_mis").hide();
    }

     function cancel_mis(){
        $(".mis").prop('readonly' , true);
        $("#btn_save_mis").hide();
        $("#btn_cancel_mis").hide();
        $("#btn_edit_mis").show();
    }
</script>