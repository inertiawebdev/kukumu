<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Leaeaf </h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Leaeaf </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
      
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title">Upload Video Play</h4> -->
                    <h6 class="card-subtitle">  </h6>
                    <form class="m-t-30" action="<?= base_url() ?>Leaeaf/update/" method="post">
                        <div class="form-group">
                            <button  type="button" onclick="window.location.href='<?= base_url() ?>Leaeaf/'" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel 
                            </button>
                           
                        </div>

                        <div class="form-group">
                            <label>Description Intro</label>
                            <textarea name="desc" class="form-control ckeditor" ><?= $leaeaf->description ?></textarea>
                        </div>

                        <div class="form-group">
                            <label >First Leaeaf Detail 1</label>
                            <input class="form-control" type="text" value="<?= $leaeaf->desc_1 ?>" name="desc_1">
                        </div>

                        <div class="form-group">
                            <label >First Leaeaf Detail 2</label>
                            <input class="form-control" type="text" value="<?= $leaeaf->desc_2 ?>" name="desc_2">
                        </div>

                        <div class="form-group">
                            <label >First Leaeaf Detail 3</label>
                            <input class="form-control" type="text" value="<?= $leaeaf->desc_3 ?>" name="desc_3">
                        </div>

                        <div class="form-group">
                            <label >First Leaeaf Detail 4</label>
                            <input class="form-control" type="text" value="<?= $leaeaf->desc_4 ?>" name="desc_4">
                        </div>

                        <button id="btn_save" type="submit" class="btn btn-success">Simpan Perubahan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  