    
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Keperluanku Intro</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Keperluanku Intro </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
      
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title">Upload Video Play</h4> -->
                    <h6 class="card-subtitle">  </h6>
                    <form class="m-t-30" action="<?= base_url() ?>Intro/update_process/" method="post" >
                        <div class="form-group">
                            <button  type="button" onclick="window.location.href='<?= base_url() ?>Intro/index/' " id="btn_edit" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel 
                            </button>
                            
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label>Description 1</label>
                            </div>
                            <div class="col-md-9">
                                <textarea name="desc1" class="form-control ckeditor" id="ckeditor"><?= $intro->desc1 ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label>Description 2</label>
                            </div>
                            <div class="col-md-9">
                                <textarea name="desc2" class="form-control ckeditor" id="ckeditor"><?= $intro->desc2 ?></textarea>
                            </div>
                        </div>

                        <button  type="submit" class="btn btn-info">
                            <i class="fa fa-save"></i> Simpan Perubahan 
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  