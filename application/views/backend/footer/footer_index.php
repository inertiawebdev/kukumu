	<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Footer </h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Footer </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
      
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-subtitle">  </h6>
                    <form class="m-t-30" action="<?= base_url() ?>Footer/update/" method="post" >
                        <div class="form-group">
                            <button  type="button" onclick="edit_mode()" id="btn_edit" class="btn btn-success">
                                <i class="fa fa-edit"></i> Edit Vision
                            </button>
                            <button  type="button" onclick="cancel()" id="btn_x" style="display:none;" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                           
                        </div>

                        <div class="form-group">
                            <label>Tokopedia</label>
                            <input type="text" readonly class="form-control" name="tokped_link" value="<?= $foot->tokped_link ?>">
                        </div>
                        
                        <div class="form-group">
                            <label>Instagram</label>
                            <input type="text" readonly class="form-control" name="instagram_link" value="<?= $foot->ig_link ?>">
                        </div>
                        
                        <div class="form-group">
                            <label>Website</label>
                            <input type="text" readonly class="form-control" name="web_link" value="<?= $foot->website ?>">
                        </div>
                        
                        <div class="form-group">
                            <label>Mail</label>
                            <input type="mail" readonly class="form-control" name="mail_link" value="<?= $foot->mail ?>">
                        </div>
                        

                        <button  style="display:none;" id="btn_save" class="btn btn-info" type="submit">
                            <i class="fas fa-save"></i> Simpan Perubahan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    
</div>  

<script type="text/javascript">
   	
   	function edit_mode(){
   		$("#btn_x").show();
   		$("#btn_edit").hide();
   		$(".form-control").attr('readonly' , false);
   		$("#btn_save").show();
   	}

   	function cancel(){
   		$("#btn_x").hide();
   		$("#btn_edit").show();
   		$(".form-control").attr('readonly' , true);
   		$("#btn_save").hide();	
   	}
</script>