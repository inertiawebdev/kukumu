<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Campaign</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Campaign </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-xl-3">
                        <!-- Nav tabs -->
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" data-toggle="pill" href="#content1" role="tab" aria-controls="v-pills-home" aria-selected="true">Head Campaign</a>
                            <a class="nav-link" data-toggle="pill" href="#content10" role="tab" aria-controls="v-pills-home" aria-selected="true">Content Video</a>
                            <a class="nav-link" data-toggle="pill" href="#content2" role="tab" aria-controls="v-pills-home" aria-selected="true">Content 1</a>
                            <a class="nav-link" data-toggle="pill" href="#content3" role="tab" aria-controls="v-pills-home" aria-selected="true">Content 2</a>
                            <a class="nav-link" data-toggle="pill" href="#content4" role="tab" aria-controls="v-pills-home" aria-selected="true">Content 3</a>
                            <a class="nav-link" data-toggle="pill" href="#content5" role="tab" aria-controls="v-pills-home" aria-selected="true">Content 4</a>
                            <a class="nav-link" data-toggle="pill" href="#content6" role="tab" aria-controls="v-pills-home" aria-selected="true">Content 5</a>
                            <a class="nav-link" data-toggle="pill" href="#content7" role="tab" aria-controls="v-pills-home" aria-selected="true">Content 6</a>
                            <a class="nav-link" data-toggle="pill" href="#content8" role="tab" aria-controls="v-pills-home" aria-selected="true">Content 7</a>
                            <a class="nav-link" data-toggle="pill" href="#content9" role="tab" aria-controls="v-pills-home" aria-selected="true">Content 8</a>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xl-9">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="content1" role="tabpanel">
                                <div class="row">
                                    <div class="form-group">
                                        <form method="post" enctype="multipart/form-data" action="<?= base_url() ?>Campaign/update/1">
                                        <div class="form-group">
                                            <button onclick="edit_mode(1)" type="button" id="edit1" class="btn btn-success">
                                                <i class="fa fa-edit"></i> Edit 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button onclick="cancel_mode(1)" style="display:none;" type="button" id="cancel1" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button style="display:none;" type="submit" id="save1" class="btn btn-info">
                                                <i class="fa fa-edit"></i> Update Perubahan 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">File Gambar</label>
                                            <input readonly type="file" class="form-control" name="file1" id="file1" >
                                        </div>

                                        <div class="form-group">
                                            <img src="<?= base_url() ?>assets/img/<?= $pic1->file ?>" style="width:540px;height: 290px;">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="tab-pane fade" id="content10" role="tabpanel" aria-labelledby="v-pills-profile-tab">.
                                <div class="row">
                                    <div class="form-group">
                                        <form method="post" enctype="multipart/form-data" action="<?= base_url() ?>Campaign/upload_video/">
                                        <div class="form-group">
                                            <button onclick="edit_mode(10)" type="button" id="edit10" class="btn btn-success">
                                                <i class="fa fa-edit"></i> Edit 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button onclick="cancel_mode(10)" style="display:none;" type="button" id="cancel10" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button style="display:none;" type="submit" id="save10" class="btn btn-info">
                                                <i class="fa fa-edit"></i> Update Perubahan 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">File Gambar</label>
                                            <input readonly type="file" class="form-control" name="file10" id="file10" >
                                        </div>

                                        <div class="form-group">
                                            <!-- <img src="<?= base_url() ?>assets/img/<?= $pic1->file ?>" style="width:540px;height: 290px;"> -->
                                            <iframe src="<?= base_url() ?>assets/video/<?= $vid10->file ?>"></iframe>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="content2" role="tabpanel" aria-labelledby="v-pills-profile-tab">.
                                <div class="row">
                                    <div class="form-group">
                                        <form method="post" enctype="multipart/form-data"   action="<?= base_url() ?>Campaign/update/2">
                                        <div class="form-group">
                                            <button onclick="edit_mode(2)" type="button" id="edit2" class="btn btn-success">
                                                <i class="fa fa-edit"></i> Edit 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button onclick="cancel_mode(2)" style="display:none;" type="button" id="cancel2" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button style="display:none;" type="submit" id="save2" class="btn btn-info">
                                                <i class="fa fa-edit"></i> Update Perubahan 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">File Gambar</label>
                                            <input readonly type="file" class="form-control" name="file2" id="file2" >
                                        </div>

                                        <div class="form-group">
                                            <img src="<?= base_url() ?>assets/img/<?= $pic2->file ?>" style="width:240px;height: 340px;">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="content3" role="tabpanel" aria-labelledby="v-pills-profile-tab">.
                                   <div class="row">
                                        <div class="form-group">
                                            <form method="post" enctype="multipart/form-data"   action="<?= base_url() ?>Campaign/update/3">
                                            <div class="form-group">
                                                <button onclick="edit_mode(3)" type="button" id="edit3" class="btn btn-success">
                                                    <i class="fa fa-edit"></i> Edit 
                                                </button>
                                            </div>
                                            <div class="form-group">
                                                <button onclick="cancel_mode(3)" style="display:none;" type="button" id="cancel3" class="btn btn-danger">
                                                    <i class="fa fa-times"></i> Cancel
                                                </button>
                                            </div>
                                            <div class="form-group">
                                                <button style="display:none;" type="submit" id="save3" class="btn btn-info">
                                                    <i class="fa fa-edit"></i> Update Perubahan 
                                                </button>
                                            </div>
                                            <div class="form-group">
                                                <label for="title">File Gambar</label>
                                                <input readonly type="file" class="form-control" name="file3" id="file3" >
                                            </div>

                                            <div class="form-group">
                                                <img src="<?= base_url() ?>assets/img/<?= $pic3->file ?>" style="width:240px;height: 340px;">
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                            </div>
                            <div class="tab-pane fade" id="content4" role="tabpanel" aria-labelledby="v-pills-profile-tab">.
                                <div class="row">
                                    <div class="form-group">
                                        <form method="post" enctype="multipart/form-data"   action="<?= base_url() ?>Campaign/update/4">
                                        <div class="form-group">
                                            <button onclick="edit_mode(4)" type="button" id="edit4" class="btn btn-success">
                                                <i class="fa fa-edit"></i> Edit 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button onclick="cancel_mode(4)" style="display:none;" type="button" id="cancel4" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button style="display:none;" type="submit" id="save4" class="btn btn-info">
                                                <i class="fa fa-edit"></i> Update Perubahan 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">File Gambar</label>
                                            <input readonly type="file" class="form-control" name="file4" id="file4" >
                                        </div>

                                        <div class="form-group">
                                            <img src="<?= base_url() ?>assets/img/<?= $pic4->file ?>" style="width:240px;height: 340px;">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="content5" role="tabpanel" aria-labelledby="v-pills-profile-tab">.
                                <div class="row">
                                    <div class="form-group">
                                        <form action="<?= base_url() ?>Campaign/update/5" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <button onclick="edit_mode(5)" type="button" id="edit5" class="btn btn-success">
                                                <i class="fa fa-edit"></i> Edit 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button onclick="cancel_mode(5)" style="display:none;" type="button" id="cancel5" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button style="display:none;" type="submit" id="save5" class="btn btn-info">
                                                <i class="fa fa-edit"></i> Update Perubahan 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">File Gambar</label>
                                            <input readonly type="file" class="form-control" name="file5" id="file5" >
                                        </div>

                                        <div class="form-group">
                                            <img src="<?= base_url() ?>assets/img/<?= $pic5->file ?>" style="width:240px;height: 340px;">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="content6" role="tabpanel" aria-labelledby="v-pills-profile-tab">.
                                <div class="row">
                                    <div class="form-group">
                                        <form method="post" enctype="multipart/form-data"   action="<?= base_url() ?>Campaign/update/6">
                                        <div class="form-group">
                                            <button onclick="edit_mode(6)" type="button" id="edit6" class="btn btn-success">
                                                <i class="fa fa-edit"></i> Edit 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button onclick="cancel_mode(6)" style="display:none;" type="button" id="cancel6" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button style="display:none;" type="submit" id="save6" class="btn btn-info">
                                                <i class="fa fa-edit"></i> Update Perubahan 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">File Gambar</label>
                                            <input readonly type="file" class="form-control" name="file6" id="file6" >
                                        </div>

                                        <div class="form-group">
                                            <img src="<?= base_url() ?>assets/img/<?= $pic6->file ?>" style="width:240px;height: 340px;">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="content7" role="tabpanel" aria-labelledby="v-pills-profile-tab">.
                                <div class="row">
                                    <div class="form-group">
                                        <form method="post" enctype="multipart/form-data"   action="<?= base_url() ?>Campaign/update/7">
                                        <div class="form-group">
                                            <button onclick="edit_mode(7)" type="button" id="edit7" class="btn btn-success">
                                                <i class="fa fa-edit"></i> Edit 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button onclick="cancel_mode(7)" style="display:none;" type="button" id="cancel7" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button style="display:none;" type="submit" id="save7" class="btn btn-info">
                                                <i class="fa fa-edit"></i> Update Perubahan 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">File Gambar</label>
                                            <input readonly type="file" class="form-control" name="file7" id="file7" >
                                        </div>

                                        <div class="form-group">
                                            <img src="<?= base_url() ?>assets/img/<?= $pic7->file ?>" style="width:240px;height: 340px;">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="content8" role="tabpanel" aria-labelledby="v-pills-profile-tab">.
                                <div class="row">
                                    <div class="form-group">
                                        <form method="post" enctype="multipart/form-data" action="<?= base_url() ?>Campaign/update/8">
                                        <div class="form-group">
                                            <button onclick="edit_mode(8)" type="button" id="edit8" class="btn btn-success">
                                                <i class="fa fa-edit"></i> Edit 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button onclick="cancel_mode(8)" style="display:none;" type="button" id="cancel8" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button style="display:none;" type="submit" id="save8" class="btn btn-info">
                                                <i class="fa fa-edit"></i> Update Perubahan 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">File Gambar</label>
                                            <input readonly type="file" class="form-control" name="file8" id="file8" >
                                        </div>

                                        <div class="form-group">
                                            <img src="<?= base_url() ?>assets/img/<?= $pic8->file ?>" style="width:240px;height: 340px;">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="content9" role="tabpanel" aria-labelledby="v-pills-profile-tab">.
                                <div class="row">
                                    <div class="form-group">
                                        <form  method="post" enctype="multipart/form-data"  action="<?= base_url() ?>Campaign/update/9">
                                        <div class="form-group">
                                            <button onclick="edit_mode(9)" type="button" id="edit9" class="btn btn-success">
                                                <i class="fa fa-edit"></i> Edit 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button onclick="cancel_mode(9)" style="display:none;" type="button" id="cancel9" class="btn btn-danger">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <button style="display:none;" type="submit" id="save9" class="btn btn-info">
                                                <i class="fa fa-edit"></i> Update Perubahan 
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <label for="title">File Gambar</label>
                                            <input readonly type="file" class="form-control" name="file9" id="file9" >
                                        </div>

                                        <div class="form-group">
                                            <img src="<?= base_url() ?>assets/img/<?= $pic9->file ?>" style="width:240px;height: 340px;">
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>  
                
<script type="text/javascript">
    function edit_mode(id){

        $("#cancel"+id).show();
        $("#save"+id).show();
        $("#edit"+id).hide();
        $("#file"+id).attr('readonly' , false);

    }

    function cancel_mode(id){

        $("#cancel"+id).hide();
        $("#save"+id).hide();
        $("#edit"+id).show();
        $("#file"+id).attr('readonly' , true);

    }
</script>