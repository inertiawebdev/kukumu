<aside class="left-sidebar" data-sidebarbg="skin2">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                
                <!-- User Profile-->
                <li class="nav-small-cap">
                    <i class="mdi mdi-dots-horizontal"></i>
                    <span class="hide-menu"> Menus</span>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url() ?>Dashboard" aria-expanded="false">
                        <i class="fas fa-globe"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url() ?>Firstpage" aria-expanded="false">
                        <i class="fa fa-desktop"></i>
                        <span class="hide-menu"> Page Awal </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url() ?>Home" aria-expanded="false">
                        <i class="fas fa-home"></i>
                        <span class="hide-menu"> Home </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-seedling"></i>
                        <span class="hide-menu"> Stories </span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?= base_url() ?>Storieshead" class="sidebar-link">
                                <i class="icon-Record"></i>
                                <span class="hide-menu"> Header  </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?= base_url() ?>Vision" class="sidebar-link">
                                <i class="icon-Record"></i>
                                <span class="hide-menu"> Vision & Mission  </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?= base_url() ?>Typhography" class="sidebar-link">
                                <i class="icon-Record"></i>
                                <span class="hide-menu"> Typhography </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url() ?>Studio" aria-expanded="false">
                        <i class="fas fa-hospital"></i>
                        <span class="hide-menu"> Studio </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url() ?>Myclass" aria-expanded="false">
                        <i class="far fa-building"></i>
                        <span class="hide-menu"> Kelasku </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fas fa-shopping-basket"></i>
                        <span class="hide-menu">Keperluanku </span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?= base_url() ?>Intro" class="sidebar-link">
                                <i class="mdi mdi-book-multiple"></i>
                                <span class="hide-menu"> Intro </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?= base_url() ?>Needshead" class="sidebar-link">
                                <i class="mdi mdi-book-plus"></i>
                                <span class="hide-menu"> Header </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?= base_url() ?>Leaeaf" class="sidebar-link">
                                <i class="mdi mdi-book-plus"></i>
                                <span class="hide-menu"> Leaef Gel </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?= base_url() ?>Cdn" class="sidebar-link">
                                <i class="mdi mdi-book-plus"></i>
                                <span class="hide-menu"> CDN </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url() ?>Campaign" aria-expanded="false">
                        <i class="fas fa-thumbtack"></i>
                        <span class="hide-menu"> Campaign </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url() ?>Footer" aria-expanded="false">
                        <i class="far fa-list-alt"></i>
                        <span class="hide-menu"> Footer </span>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>