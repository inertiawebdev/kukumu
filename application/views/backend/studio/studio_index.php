<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Studioku</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?= base_url() ?>Dashboard">Beranda</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"> Studioku </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
      
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title">Mission</h4> -->
                    <h6 class="card-subtitle">  </h6>
                    <form class="m-t-30" action="<?= base_url() ?>Studio/update" method="post" >
                        <div class="form-group">
                            <button  type="button" onclick="edit_stu()" id="btn_edit_mis" class="btn btn-success">
                                <i class="fa fa-edit"></i> Edit Mission
                            </button>
                            <button  type="button" onclick="cancel_stu()" id="btn_cancel_mis" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                           
                        </div>


                        <div class="form-group">
                            <label>First Address</label>
                            <textarea rows="6" readonly name="studio1" class="form-control" ><?= $studio->first_studio ?></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label>First Phone Number</label>
                            <input type="text" readonly class="form-control" name="phone1" value="<?= $studio->first_phone ?>">
                        </div>

                        <div class="form-group">
                            <label>Second Address</label>
                            <textarea rows="6" readonly name="studio2" class="form-control mis" ><?= $studio->second_studio ?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Second Phone Number</label>
                            <input type="text" readonly class="form-control" name="phone2" value="<?= $studio->second_phone ?>">
                        </div>



                        
                        <button  style="display:none;" id="btn_save_mis" class="btn btn-info" type="submit">
                            <i class="fas fa-save"></i> Simpan Perubahan
                        </button>
                        

                        

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>  

<script type="text/javascript">
    $(document).ready(function($) {
        
        $(".vis").prop('readonly' , true);
        $(".mis").prop('readonly' , true);
        $("#btn_cancel_vis").hide();
        $("#btn_cancel_mis").hide();
    });

    function edit_stu(){
        $(".form-control").prop('readonly' , false);
        $("#btn_save_mis").show();
        $("#btn_cancel_mis").show();
        $("#btn_edit_mis").hide();
    }

     function cancel_stu(){
        $(".form-control").prop('readonly' , true);
        $("#btn_save_mis").hide();
        $("#btn_cancel_mis").hide();
        $("#btn_edit_mis").show();
    }

   
</script>