    
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Home Page</h4>
                <div class="d-flex align-items-center">

                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<?= base_url() ?>Dashboard">Beranda</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"> Home </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
          
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h4 class="card-title">Upload Video Play</h4> -->
                        <h6 class="card-subtitle">  </h6>
                        <form class="m-t-30" action="<?= base_url() ?>Home/update_data/" method="post" >
                            <div class="form-group">
                                <button  type="button" onclick="window.location.href='<?= base_url() ?>Home/index' " id="btn_cancel" class="btn btn-danger">
                                <i class="fa fa-times"></i> Cancel 
                            </button>
                            </div>

                            <div class="form-group">
                            <label for="title">Title</label>
                                <input type="text"  value="<?= $home->headline ?>" class="form-control" name="headline" id="title" placeholder="ex: Welcome to Kukumu">
                                
                            </div>

                            <div class="form-group">
                                <label for="title">Description</label>
                                <textarea name="desc" class="form-control ckeditor" id="ckeditor"><?= $home->description ?></textarea>
                            </div>
                            
                            <button id="btn_save" type="submit" class="btn btn-success">Simpan Perubahan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>  