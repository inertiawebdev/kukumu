
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Play Video</h4>
                <div class="d-flex align-items-center">

                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<?= base_url() ?>Dashboard">Beranda</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"> Firstpage </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <iframe class="col-md-12" style="height:400px;" src="<?= base_url() ?>assets/video/<?= $video->video_directory ?>"></iframe>
                    </div>
                </div>
            </div>
        </div>  
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Upload Video Play</h4>
                        <h6 class="card-subtitle">  </h6>
                        <form class="m-t-30" action="<?= base_url() ?>Firstpage/upload_video/" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Upload Video</label>
                                <input type="file" class="form-control" name="video" id="exampleInputEmail1" placeholder="Enter email">
                                <small class="form-text text-danger">Video Maks 8MB . Format Mp4 /WebM / Ogg</small>
                            </div>
                            
                            <button type="submit" class="btn btn-success">Simpan Perubahan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>  
